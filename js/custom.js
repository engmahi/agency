jQuery(function ($) {

    // -------------------------------------------------------------
    //  ScrollUp 
    //  Sticky Menu 
    //  Magnific Popup
    //  One Page Nav 
    //  Isotope Active 
    //  BX Slider 
    // -------------------------------------------------------------
   

    // -------------------------------------------------------------
    //  ScrollUp 
    // -------------------------------------------------------------

    
    (function () {
    
        $.scrollUp();

    }()); 


    // -------------------------------------------------------------
    //  Sticky Menu
    // -------------------------------------------------------------
    
        (function () {
        
                 var nav = $('#header .navbar');
                 var scrolled = false;

                 $(window).scroll(function () {

                     if (200 < $(window).scrollTop() && !scrolled) {
                         nav.addClass('sticky animated fadeInDown').animate({ 'margin-top': '0px' });

                         scrolled = true;
                     }

                     if (200 > $(window).scrollTop() && scrolled) {
                         nav.removeClass('sticky animated fadeInDown').css('margin-top', '0px');

                         scrolled = false;
                     }
                 });

        }());

    // -------------------------------------------------------------
    //  Magnific Popup
    // ------------------------------------------------------------- 

        (function () {

            $('.portfolio-item a').magnificPopup({
                type: 'image',
                gallery:{
                enabled:true
                }
            });

        }());         

    // -------------------------------------------------------------
    //  One Page Nav
    // ------------------------------------------------------------- 

    
        (function () {

            $('.nav').onePageNav({
                currentClass: 'active',
                changeHash: false,
                scrollSpeed: 750,
                scrollThreshold: 0.5,
                filter: '',
                easing: 'swing'
            });

        }());

    // -------------------------------------------------------------
    //  Isotope Active
    // -------------------------------------------------------------

    (function () {

        var winDow = $(window);

        // Needed customize these variables
        var $container=$('.portfolio-container');   // Portfolio Container Class or ID
        var $filter=$('.portfolio-filter');         // Portfolio Filter UL Class or ID
        var filterItemA = $('.portfolio-filter a'); // Portfolio Filter UL anchor (a)

        try{
            $container.imagesLoaded( function(){

                $container.show();
                $container.isotope({
                    filter:'*',
                    layoutMode:'masonry',
                    animationOptions:{
                        duration:750,
                        easing:'linear'
                    }
                });
            });
        } catch(err) {
        }

        winDow.bind('resize', function(){
            var selector = $filter.find('a.active').attr('data-filter');

            try {
                $container.isotope({ 
                    filter  : selector,
                    animationOptions: {
                        duration: 750,
                        easing  : 'linear',
                        queue   : false,
                    }
                });
            } catch(err) {
            }
            return false;
        });
        
        // Isotope Filter 
        $filter.find('a').click(function(){
            var selector = $(this).attr('data-filter');

            try {
                $container.isotope({ 
                    filter  : selector,
                    animationOptions: {
                        duration: 750,
                        easing  : 'linear',
                        queue   : false,
                    }
                });
            } catch(err) {

            }
            return false;
        });


        filterItemA.on('click', function(){
            var $this = $(this);
            if ( !$this.hasClass('active')) {
                filterItemA.removeClass('active');
                $this.addClass('active');
            }
        });

    }());       


});

